词性分析
=============

词性分析包含中文分词和词性标注两大模块。

分词
-------------------

分词是中文自然语言处理的基本任务，其目标是把文本以词为单位进行分割，并返回JSON格式的分析结果。

分词任务的URI为::

    http://api.ict-lap.com/analysis/seg/

分词任务的调用示例如下::

    $ curl -X POST -H "Content-Type: application/json" -H "Accept: application/json" \
    --data "\"我是中国人。\"" \
    http://api.ict-lap.com/analysis/seg/

结果返回为::

    HTTP/1.1 200 OK
    Server: nginx/1.1.19
    Date: **** ** **
    Content-Type: application/javascript
    Transfer-Encoding: chunked
    Connection: keep-alive
    Vary: Accept-Encoding
    
    [{"words":["我","是","中国人","。"]}]


词性标注
----------------------

词性标注结果是将文本分词后进行词性类别的标注，并返回JSON格式的分析结果。如果分析文本已进行分词Server将不再分词。

词性标注任务的调用示例如下::

    $ curl -X POST -H "Content-Type: application/json" -H "Accept: application/json" \
    --data "\"我是中国人。\"" \
    http://api.ict-lap.com/analysis/pos/

返回结果为::

    HTTP/1.1 200 OK
    Server: nginx/1.1.19
    Date: **** ** **
    Content-Type: application/json
    Transfer-Encoding: chunked
    Connection: keep-alive
    Vary: Accept-Encoding
    [{"tags":["r","v","ns","n","wp"],
     "words":["我","是","中国人","。"]}]







