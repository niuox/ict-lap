.. ICT-LAP documentation master file, created by
   sphinx-quickstart on Mon Aug 17 15:03:56 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ICT-LAP's RESTful API documentation!
===============================================

HTTP消息结构
===================

.. toctree::
    :maxdepth: 2

    http

组件任务
============

.. toctree::
    :maxdepth: 2

    lexical
    ner
    snippet
    topic
    sentiment

